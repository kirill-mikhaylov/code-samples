<?php

namespace App\Http;

use App\Http\Inventories\Local;
use App\Http\Inventories\Shopify;
use App\Http\Inventories\Vend;

class Inventory {

    private static $_instance        = null;
    private static $_activeInventory = null;

    /**
     * Inventory constructor.
     * @param string $inventory
     */
    public function __construct($inventory = 'local')
    {
        //
    }

    /**
     * Allows to specify which inventory to use.
     * @param null $inventory
     * @return null|static
     */
    public static function by($inventory = null)
    {
        // Set itself
        if (self::$_instance === null) {
            self::$_instance = new static;
        }

        // Select inventory
        if (!is_null($inventory)) {
            self::$_activeInventory = $inventory;
        } else {
            self::$_activeInventory = 'local';
        }

        // Return instance
        return self::$_instance;
    }

    /**
     * Get product(-s) for the active inventory
     * @param null $productId
     * @return array|mixed|void
     */
    public static function getProducts($productId = null)
    {
        // Check active inventory and set it to local when null
        if (is_null(self::$_activeInventory)) {
            self::$_activeInventory = 'local';
        }

        // Get products based on selected inventory
        switch (strtolower(self::$_activeInventory)) {

            case 'local' :
                $inventory = new Local();
                return $inventory->getProducts($productId);

            case 'shopify' :
                $inventory = new Shopify();
                return $inventory->getProducts($productId);

            case 'vend' :
                $inventory = new Vend();
                return $inventory->getProducts($productId);

            default :
                return [];
        }
    }

}
