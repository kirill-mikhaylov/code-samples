<?php

namespace App\Http\Inventories;

use App\Http\Inventories\Interfaces\InventoryContract;
use Illuminate\Support\Facades\DB;

class Local implements InventoryContract
{
    /**
     * Local constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get products
     * @param null $productId
     * @return array|mixed
     */
    public function getProducts($productId = null)
    {
        // Get products or product
        if (is_null($productId)) {
            $response = DB::select("SELECT * FROM inventory");
        } else {
            $response = DB::select("SELECT * FROM inventory WHERE id = :id", ['id' => $productId]);
        }

        // Format response
        $formattedResponse = $this->formatResponse($response);

        // Return response
        return $formattedResponse;
    }

    /**
     * Format response into standard output
     * @param null $response
     * @return array
     */
    private function formatResponse($response = null)
    {
        if (!is_array($response)) {
            return [];
        }

        $products = [];

        foreach ($response as $product) {
            $products[strtoupper(strval($product->sku))] = [
                'id'       => $product->id,
                'name'     => $product->name,
                'variant'  => $product->variant,
                'price'    => $product->price,
                'sku'      => $product->sku,
                'quantity' => $product->quantity
            ];
        }

        return $products;
    }
}
