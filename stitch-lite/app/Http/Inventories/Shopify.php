<?php

namespace App\Http\Inventories;

use App\Http\Remote;
use App\Http\Inventories\Interfaces\InventoryContract;

class Shopify implements InventoryContract
{
    private $apiUrl = null;

    /**
     * Shopify constructor.
     */
    public function __construct()
    {
        $this->apiUrl = 'https://'
            . config('inventories.channels.shopify.api_key') . ':'
            . config('inventories.channels.shopify.api_pwd') . '@'
            . config('inventories.channels.shopify.store_name') . '.myshopify.com/admin/';
    }

    /**
     * Get products
     * @param null $productId
     * @return array|mixed
     */
    public function getProducts($productId = null)
    {
        // Get products
        $response = Remote::request('GET', $this->apiUrl . 'products.json');

        // Format response
        $formattedResponse = $this->formatResponse($response);

        // Return response
        return $formattedResponse;
    }

    /**
     * Format response into standard output
     * @param null $response
     * @return array
     */
    private function formatResponse($response = null)
    {
        if (!is_array($response) || !isset($response['data']['products'])) {
            return [];
        }

        $products = [];

        foreach ($response['data']['products'] as $index => $product) {
            foreach ($product['variants'] as $variant) {
                $products[strtoupper(strval($variant['sku']))] = [
                    'name'     => $product['title'],
                    'variant'  => $variant['title'],
                    'price'    => number_format($variant['price'], 2, '.', ''),
                    'sku'      => $variant['sku'],
                    'quantity' => $variant['inventory_quantity']
                ];
            }
        }

        return $products;
    }
}
