<?php

namespace App\Http\Inventories;

use App\Http\Inventories\Interfaces\InventoryContract;
use VendAPI\VendAPI;

class Vend implements InventoryContract
{
    private $vendApi = null;

    /**
     * Vend constructor.
     * API wrapper used from: https://github.com/brucealdridge/VendAPI
     */
    public function __construct()
    {
        $this->vendApi = new VendAPI(
            'https://' . config('inventories.channels.vend.store_name') . '.vendhq.com',
            'Bearer',
            config('inventories.channels.vend.store_access_token')
        );
        $this->vendApi->automatic_depage = true;
    }

    /**
     * Get products
     * @param null $productId
     * @return array|mixed
     */
    public function getProducts($productId = null)
    {
        // Get products
        $response = $this->vendApi->getProducts(array('active' => '1'));

        // Format response
        $formattedResponse = $this->formatResponse($response);

        // Return response
        return $formattedResponse;
    }

    /**
     * Format response into standard output
     * @param null $response
     * @return array
     */
    private function formatResponse($response = null)
    {
        // Make response is not null
        if (!is_array($response)) {
            return [];
        }

        // Preset formatted products dictionary
        $products = [];

        // Iterate through products
        foreach ($response as $index => $product) {

            // Convert Vend Product to an array
            $productArray = $product->toArray();

            // Skip any products that are not tracking inventory
            if (!$productArray['track_inventory']) {
                continue;
            }

            // Check and format variant
            $variant = '';
            if (mb_strpos($productArray['name'], '/') !== false) {
                $variant = array_map('trim', explode('/', $productArray['name']));
                unset($variant[0]);
                $variant = implode(' / ', $variant);
            }

            // Add product
            $products[strtoupper(strval($productArray['sku']))] = [
                'name'     => $productArray['base_name'],
                'variant'  => $variant,
                'price'    => number_format($productArray['price'], 2, '.', ''),
                'sku'      => $productArray['sku'],
                'quantity' => $product->getInventory()
            ];
        }

        return $products;
    }
}
