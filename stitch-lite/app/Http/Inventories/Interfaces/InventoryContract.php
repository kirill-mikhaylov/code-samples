<?php

namespace App\Http\Inventories\Interfaces;

interface InventoryContract
{
    /**
     * Gets a response containing a list of products or a single product if optional productId is specified
     * @param null $productId
     * @return mixed
     */
    public function getProducts($productId = null);
}
