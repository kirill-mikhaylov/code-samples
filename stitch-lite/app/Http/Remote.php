<?php

namespace App\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;

class Remote {

    public function __construct() {}

    /**
     * Make a request
     *
     * @param null $method
     * @param null $endPoint
     * @param array $data
     * @param array $headers
     * @return array|null
     */
    public static function request($method = null, $endPoint = null, $data = [], $headers = [])
    {
        // Check parameters
        if (is_null($method) || is_null($endPoint)) {
            return null;
        }

        // Try to make a request
        try {

            // Setup Guzzle client
            $client = new GuzzleClient();

            // Make a request
            $response = $client->request(strtoupper($method), $endPoint, [
                'headers'     => $headers,
                'form_params' => $data,
            ]);

            // Handle and return response
            return self::handleResponse($response);

        } catch (RequestException $e) {

            // Check for a response object
            if ($e->hasResponse()) {

                // Get response
                $response = $e->getResponse();

                // Handle and return response
                return self::handleResponse($response);
            }
        }
    }

    /**
     * Return formatted response object
     *
     * @param null $response
     * @return array|null
     */
    private static function handleResponse($response = null)
    {
        // Check parameters
        if (is_null($response)) {
            return null;
        }

        // Get the response code and reason
        $code   = $response->getStatusCode();
        $reason = $response->getReasonPhrase();


        // Get response data
        $body         = $response->getBody();
        $bodyContents = $body->getContents();
        $response     = json_decode($bodyContents, true);

        // Construct and return a response object
        return [
            'code'   => $code,
            'reason' => $reason,
            'data'   => $response
        ];
    }
}
