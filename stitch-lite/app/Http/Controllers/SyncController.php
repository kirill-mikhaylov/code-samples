<?php

namespace App\Http\Controllers;

use App\Http\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SyncController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function post (Request $request)
    {
        // Get sync direction (UP - to channels; DOWN - to Stitch Lite)
        if (!$request->has('direction')) {
            $direction = 'DOWN';
        } else {
            $direction = strtoupper($request->input('direction'));
            $direction = (in_array($direction, ['DOWN', 'UP'])) ? $direction : 'DOWN';
        }

        // Sync based on direction
        switch ($direction) {

            case 'UP' :
                return $this->syncUp();

            case 'DOWN' :
            default     :
                return $this->syncDown();
        }
    }

    /**
     * Sync up inventory to channels (from this point on inventory is being managed exclusively through Stitch)
     * @return $this
     */
    private function syncUp ()
    {
        // TODO: Extra points to sync inventory up to channels

        // Return response
        return response('', 501)->header('Content-Type', 'application/json');
    }

    /**
     * Sync down inventory from channels (most likely case scenario running it for the first time to seed the data)
     * @return $this
     */
    private function syncDown ()
    {
        // Get all the inventory channels from settings
        $channels = array_keys(config('inventories.channels'));

        // Get all the local products
        $productsLocal = Inventory::getProducts();

        // Preset default collectors
        $newProducts = [];
        $allChannelProducts = [];

        // Start synchronizing with each channel
        foreach ($channels as $channel) {

            // Get products
            $products = Inventory::by($channel)->getProducts();

            // Iterate through products
            foreach ($products as $sku => $product) {

                // Make a local product copy and match channel format
                if (isset($productsLocal[$sku])) {
                    $productLocalCopy = $productsLocal[$sku];
                    unset($productLocalCopy['id'], $productLocalCopy['quantity']);
                }

                // Quantity will be addressed separately
                $productCopy = $product;
                unset($productCopy['quantity']);

                // Check if SKU of a product is not in the system and add that product to the list of new products
                if (!array_key_exists($sku, $productsLocal) && !array_key_exists($sku, $newProducts)) {
                    $newProducts[$sku] = [
                        'name'     => $product['name'],
                        'variant'  => $product['variant'],
                        'price'    => $product['price'],
                        'sku'      => $product['sku'],
                        'quantity' => $product['quantity']
                    ];

                    // Check if any of the values are different
                } else if (isset($productLocalCopy) && $productLocalCopy !== $productCopy) {

                    DB::table('inventory')
                        ->where('id', $productsLocal[$sku]['id'])
                        ->update($productCopy);
                }

                // Validate all channel product quantities
                if (!array_key_exists($sku, $allChannelProducts)) {
                    $allChannelProducts[$sku] = $product;
                } else {

                    // If channel quantity is less than Stitch quantity
                    if ($product['quantity'] < $allChannelProducts[$sku]['quantity']) {
                        $allChannelProducts[$sku]['quantity'] = $product['quantity'];
                    }
                }
            }
            unset($products, $product);
        }
        unset($channel);

        // Insert new products
        if (count($newProducts)) {
            DB::table('inventory')->insert($newProducts);
        }

        // Get a fresh list of products
        $productsLocal = Inventory::by('local')->getProducts();

        // Verify and update quantities
        foreach ($productsLocal as $sku => $product) {
            if ($product['quantity'] !== $allChannelProducts[$sku]['quantity']) {
                DB::table('inventory')
                    ->where('id', $product['id'])
                    ->update(['quantity' => $allChannelProducts[$sku]['quantity']]);
            }
        }

        // Return response
        return response('', 204)->header('Content-Type', 'application/json');
    }
}
