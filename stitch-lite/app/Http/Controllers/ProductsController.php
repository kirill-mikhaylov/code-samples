<?php

namespace App\Http\Controllers;

use App\Http\Inventory;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return Stitch inventory product(-s)
     * @param null $productId - optional URL param
     * @return $this
     */
    public function get ($productId = null)
    {
        // Get Stitch products
        $products = Inventory::by('local')->getProducts($productId);

        // Return response
        return response($products, 200)->header('Content-Type', 'application/json');
    }
}
