<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Get product(-s)
$router->get('/api/products[/{productId}]', 'ProductsController@get');

// Synchronize Stitch Lite inventory with 3rd parties
$router->post('/api/sync', 'SyncController@post');
