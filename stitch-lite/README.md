# Lumen - Inventory Tracking

### Overview
An inventory tracking system that integrates 2 API's and sync down inventories.

### Installation
- Clone repository
- Run: `composer install`
- Copy .env.example over to .env
- Create database as per .env specification
- Configure your local server to run Lumen + Artisan
- Run migrations: `php artisan migrate`
- Setup your private app for [Shopify](https://www.shopify.com/) and enter required credentials in .env
- Then do the same for API key and developer account with [VendHQ](https://www.vendhq.com/)
- Finally, don't forget to add similar items to both POS with same and different SKU to see it in action

### Testing functionality
- You can CURL directly in command line or use Postman to initiate calls
- There are two endpoints:
  - `/api/products` - returns a list of all the Stitch Lite products
  - `/api/products/:productId` - returns individual product information for specified :productId
  - `/api/sync` - initiates synchronization between Shopify, Vend, and Stitch Lite
  
### Instructions for this exercise
To build a lite version of inventory tracking system that integrates Shopify and Vend.
