<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Inventory
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default channel inventory that should be used
    | by the framework.
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many channel inventories as you wish.
    |
    */

    'channels' => [

        'shopify' => [
            'store_name' => env('SHOPIFY_STORE_NAME',    ''),
            'api_key'    => env('SHOPIFY_ADMIN_API_KEY', ''),
            'api_pwd'    => env('SHOPIFY_ADMIN_API_PWD', ''),
            'secret'     => env('SHOPIFY_ADMIN_SECRET',  ''),
        ],

        'vend' => [
            'store_name'         => env('VEND_STORE_NAME',         ''),
            'store_access_token' => env('VEND_STORE_ACCESS_TOKEN', ''),
        ],

    ],

];
