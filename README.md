# README #

This is a collection of sample codes that either were asked of me to do as a code exercise or created by me for the purpose of a research.

### `drupal8`: Custom Module - Donation Form ###
A simple Drupal 8 custom module for creating a donation form to collect payments using Stripe. It was done as a coding exercise.

### `stitch-lite`: Inventory Tracking ###
A 24-hour code challenge to create an inventory tracking system that integrates 2 API's and sync down inventories.
