<?php

namespace Drupal\donation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AjaxResponse;

use Stripe\Stripe as Stripe;
use Stripe\Token as StripeToken;
use Stripe\Charge as StripeCharge;

class DonationForm extends FormBase {

    /**
     * Stripe API key to test the payment processing with
     */
    const STRIPE_API_KEY = '';

    /**
     * Generic payment processing error
     */
    const MSG_ERROR_PROCESSING = 'There was a problem processing a transaction. Please try again later.';

    /**
     * Success message upon complete donation
     */
    const MSG_SUCCESS = 'Thank you for your generous donation!';

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'donation_form';
    }

    /**
     * Construct the donation form
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        // Default settings
        $config = $this->config('donation.settings');

        // Intro
        $form['help'] = [
            '#type'   => 'item',
            '#markup' => t('Donate to our early education programs because learning cannot wait.'),
        ];

        // Donation amounts
        $form['amount'] = array(
            '#type'    => 'select',
            '#title'   => $this->t('Select amount to donate'),
            '#options' => [
                '1'      => $this->t('$1.00'),
                '2'      => $this->t('$2.00'),
                '3'      => $this->t('$3.00'),
                '5'      => $this->t('$5.00'),
                '10'     => $this->t('$10.00'),
                'custom' => $this->t('Custom amount'),
            ],
        );

        // Custom amount
        $form['amount_custom'] = array(
            '#type'   => 'textfield',
            '#title'  => $this->t('Enter custom amount to donate in dollars:'),
            '#states' => array(
                'visible' => array(
                    ':input[id="edit-amount"]' => array(
                        'value' => 'custom'
                    ),
                ),
            )
        );

        // Donor Info - Fieldset
        $form['donor_info'] = array(
            '#type'     => 'fieldset',
            '#title'    => $this->t('Donor Information'),
            '#required' => true,
        );

        // Donor first name
        $form['donor_info']['first_name'] = array(
            '#type'        => 'textfield',
            '#title'       => $this->t('First name'),
            '#description' => $this->t('Please enter donor\'s first name'),
            '#prefix'      => '<div id="first-name-result"></div>',
            '#ajax'        => array(
                'callback' => 'Drupal\donation\Form\DonationForm::validateDonorFirstName',
                'effect'   => 'fade',
                'event'    => 'change',
                'wrapper'  => 'first-name-result',
                'progress' => array(
                    'type'    => 'throbber',
                    'message' => NULL,
                ),
            )
        );

        // Donor last name
        $form['donor_info']['last_name'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Last name'),
            '#default_value' => $config->get('donation.last_name'),
            '#description'   => $this->t('Please enter donor\'s last name'),
            '#prefix'        => '<div id="last-name-result"></div>',
            '#ajax'          => array(
                'callback' => 'Drupal\donation\Form\DonationForm::validateDonorLastName',
                'effect'   => 'fade',
                'event'    => 'change',
                'wrapper'  => 'last-name-result',
                'progress' => array(
                    'type'    => 'throbber',
                    'message' => NULL,
                ),
            )
        );

        // CC Info - Fieldset
        $form['credit_card_info'] = array(
            '#type'     => 'fieldset',
            '#title'    => $this->t('Credit Card Information'),
            '#required' => true,
        );

        // CC Info - Number
        $form['credit_card_info']['cc_number'] = array(
            '#type'  => 'textfield',
            '#title' => $this->t('Credit Card Number')
        );

        // CC Info - Expiration month
        $form['credit_card_info']['cc_exp_month'] = array(
            '#type'    => 'select',
            '#title'   => $this->t('Expiration Month'),
            '#prefix'  => '<div class="container-inline">',
            '#options' => [],
        );
        for ($i = 1; $i <= 12; $i++) {
            $form['credit_card_info']['cc_exp_month']['#options'][$i] = $this->t(sprintf("%02d", $i));
        }

        // CC Info - Expiration Year
        $form['credit_card_info']['cc_exp_year'] = array(
            '#type'    => 'select',
            '#title'   => $this->t('Expiration Year'),
            '#prefix'  => '&nbsp;&nbsp;&nbsp;&nbsp;',
            '#options' => [],
        );
        for ($i = date('Y'); $i <= (date('Y') + 20); $i++) {
            $form['credit_card_info']['cc_exp_year']['#options'][$i] = $this->t(sprintf("%d", $i));
        }

        // CC Info - CVC
        $form['credit_card_info']['cc_cvc'] = array(
            '#type'       => 'textfield',
            '#title'      => $this->t('CVC'),
            '#prefix'     => '&nbsp;&nbsp;&nbsp;&nbsp;',
            '#suffix'     => '</div>',
            '#attributes' => array(
                'maxlength' => 4,
                'size'      => 7
            ),
        );

        // Submit button
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => $this->t('Donate'),
        );

        // Return form
        return $form;
    }

    /**
     * Validate first name
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     */
    public function validateDonorFirstName(array &$form, FormStateInterface $form_state)
    {
        $value  = $form_state->getValue('first_name');
        $result = self::validateDonorName($value);
        return self::ajaxCallback($result, '#first-name-result');
    }

    /**
     * Validate last name
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     */
    public function validateDonorLastName(array &$form, FormStateInterface $form_state)
    {
        $value  = $form_state->getValue('last_name');
        $result = self::validateDonorName($value);
        return self::ajaxCallback($result, '#last-name-result');
    }

    /**
     * General donor name validation rules
     * @param null $value
     * @return string
     */
    private static function validateDonorName ($value = null)
    {
        // Check parameters
        if (is_null($value)) {
            return '';
        }

        // Validate input value
        if (mb_strlen($value) < 1) {
            $message = 'The name cannot be empty';
        } else if (!ctype_alpha($value)) {
            $message = 'Only letter characters are allowed';
        } else {
            $message = '';
        }

        // Return message
        return $message;
    }

    /**
     * Check CC number
     * @author https://stackoverflow.com/questions/174730/what-is-the-best-way-to-validate-a-credit-card-in-php
     * @param $cc
     * @return bool|mixed
     */
    private static function checkCcNum ($cc)
    {
        $cards = array(
            "visa"       => "(4\d{12}(?:\d{3})?)",
            "amex"       => "(3[47]\d{13})",
            "jcb"        => "(35[2-8][89]\d\d\d{10})",
            "maestro"    => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
            "solo"       => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
            "mastercard" => "(5[1-5]\d{14})",
            "switch"     => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
        );
        $names   = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch");
        $matches = array();
        $pattern = "#^(?:" . implode("|", $cards) . ")$#";
        $result  = preg_match($pattern, str_replace(" ", "", $cc), $matches);
        return ($result > 0) ? $names[sizeof($matches) - 2] : false;
    }

    /**
     * AJAX validation response callback
     * @param null $message
     * @param null $output_container
     * @return AjaxResponse
     */
    private static function ajaxCallback ($message = null, $output_container = null)
    {
        // Check parameters
        if (is_null($message)) {
            $message = '';
        }

        // Construct new ajax response object
        $ajax_response = new AjaxResponse();

        // Add command
        $ajax_response->addCommand(new HtmlCommand($output_container, $message));

        // Return response
        return $ajax_response;
    }

    /**
     * Validate form
     * {@inheritdoc}
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        // Check custom donation
        if ($form_state->getValue('amount') == 'custom') {
            if (!is_numeric($form_state->getValue('amount_custom'))) {
                $form_state->setErrorByName('amount_custom', $this->t('Custom amount must be numeric'));
            } else if ($form_state->getValue('amount_custom') < 1) {
                $form_state->setErrorByName('amount_custom', $this->t('Custom amount must be greater than one dollar'));
            }
        }

        // Check first and last name
        foreach (['first_name', 'last_name'] as $fieldName) {
            if (mb_strlen($errorMsg = self::validateDonorName($form_state->getValue($fieldName))) > 0) {
                $form_state->setErrorByName($fieldName, $this->t($errorMsg));
            }
        }

        // Check credit card number
        if (self::checkCcNum($form_state->getValue('cc_number')) === false) {
            $form_state->setErrorByName('cc_number', $this->t('The credit card number seems to be incorrect'));
        }
    }

    /**
     * Process form
     * {@inheritdoc}
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        try {

            // Set API key
            Stripe::setApiKey(self::STRIPE_API_KEY);

            // Create a payment token
            $paymentToken = StripeToken::create(array(
                'card' => array(
                    'number'    => $form_state->getValue('cc_number'),
                    'exp_month' => $form_state->getValue('cc_exp_month'),
                    'exp_year'  => $form_state->getValue('cc_exp_year'),
                    'cvc'       => $form_state->getValue('cc_cvc')
                )
            ));

            // Get amount
            $amount = $form_state->getValue('amount');
            if ($amount == 'custom') {
                $amount = $form_state->getValue('amount_custom');
            }

            // Convert amount to cents
            $amount = $amount * 100;

            // Make a donation charge
            $charge = StripeCharge::create(array(
                'amount'   => $amount,
                'currency' => 'usd',
                'source'   => $paymentToken['id']
            ));

            // Report success or error
            if ($charge['status'] == 'succeeded') {
                drupal_set_message(self::MSG_SUCCESS, 'status');
            } else {
                drupal_set_message(self::MSG_ERROR_PROCESSING, 'error');
            }

        } catch (\Exception $e) {
            error_log($e->getMessage());
            drupal_set_message(self::MSG_ERROR_PROCESSING, 'error');
        }
    }
}
