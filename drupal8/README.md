# Drupal 8 - Custom Module - Donation Form 

### Overview
This is a simple Drupal 8 custom module for creating a donation form to collect payments using Stripe.

### Installation
- Clone repository
- Run: `composer install`
- Configure your local server to run Drupal 8
- Launch this demo and follow Drupal setup instructions in the browser

### Using the module
- The module is located in `\drupal8\web\modules\custom\donation`
- To test the module:
  - Login as administrator and go to go **Manage > Extend**
  - Find and select _Donation Form_ under Custom and click _Install_ at the bottom of the page
  - Go to `http://your-localhost/donation` to see the custom module in works
- To test the Stripe integration:
  - Set your test key `STRIPE_API_KEY` in `\drupal8\web\modules\custom\donation\src\Form\DonationForm.php`
  
### Instructions for this exercise
A client has asked for a simple donation form on their Drupal 8 site. The client already uses Stripe for processing payments at their events and would like to use it for the donation form as well. The client needs to collect who is making the donation, how much it’s for and of course collect the credit card payment.
